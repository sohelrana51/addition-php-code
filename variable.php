<!DOCTYPE html>
<html>
	<head>
		<title>PHP with html & css</title>
		
		<style>
			.first {
				background:#ddd;
				width: 900px;
				margin: 0 auto;
				padding: 30px;
				text-align: center;
				color: #fff;
				font-size: 30px;
				font-weight: 700;
			}
			.header, .footer {
				background: #000;
				color: #fff;
				text-align: center;
			}
			.main-content {
				min-height: 300px;
				color: #000;
				display: flex;
				flex-direction: column;
				justify-content: center;
			}
		</style>
	</head>
	<body>
		<div class="first">
		
		<section class="header">
			<h2>PHP Fundamentals</h2>
		</section>
		<section class="main-content">			
		<?php
			$a = 10;
			$b = 20;
			$c = $a+$b;
			echo "Total Value = ".$c;
		?>
		</section>
		<section class="footer">
			<h2>WWW.Makpie.com</h2>
		</section>
		</div>
	</body>
</html>